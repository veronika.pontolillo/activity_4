public class Book {
    protected String title;
    private String author;

    public Book(String title, String author){
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }

    public String toString(){
        String newStr = "Title: "+this.title+" Author: "+this.author;
        return newStr;
    }
}